# OpenWeatherMap Restful APIs

### Author : Ke (Wallace) Tan

Description: 
===
Develop and test a HTTP REST API in Node.js that fronts the OpenWeatherMap service:
OpenWeatherMap name service guide: http://openweathermap.org/current#name . (Example: http://samples.openweathermap.org/data/2.5/weather?q=London,uk)
Your service should:
1. Enforce API Key scheme. An API Key is rate limited to 5 weather reports an hour. After that your service should respond in a way which communicates that the hourly limit has been exceeded. Create 5 API Keys. Pick a convention for handling them that you like; using simple string constants is fine. This is NOT an exercise about generating and distributing API Keys. Assume that the user of your service knows about them.
2. Have a URL that accepts both a city name and country name. Based upon these inputs, and the API Key, your service should decide whether or not to call the OpenWeatherMap name service. If it does, the only weather data you need to return to the client is the description field from the weather JSON result. Whether it does or does not, it should respond appropriately to the client.
3. Reject requests with invalid input or missing API Keys.

Hypothesis 
====

* Invoke weather data in OpenWeather service.
* Use simple string as API Key.
* Only return the description of the first weather block.
* Return the error message with 200 when there is error from OpenWeather service

Design/Architecture 
====

* Use the following components in this test code project

 1. Koa 2
 2. Babel
 3. Asynchronous Functions (Async/Await)
 4. Redis
 5. Cookie
 6. Session
 7. Eslint
 8. ES6
 9. Mocha
 10. SuperTest

* Use Koa 2 web framework.
* The project structure is scalable.
* The rate limit is set for each API Key.
* The rate limit is configurable in _config.js_.
* Use redis server for rate limit. Use local redis as default. However, the redis server is configurable.
* Throw 429 for over-rate-limit situation.
* Throw 401 for unauthenticated request. 
* Add X-RateCount in the header of the response.
* There are _rountes_, _middlewares_, _controllers_ and _services_ packages.
* Throw _SubSystemError_ if there is an error from OpenWeather service.
* All configurations are in _config.js_
* The application entry is _index.js_
* The calling flow is authentication_middleware -> rateLimit_middleware -> route -> controller -> service -> external openWeather Service

## Install/Start Local Redis Server
Install Redis Server
```
brew install redis
```

Start Redis Server
```
brew services start redis
```

## Running
Install dependencies
```
npm install
```

Start a Local Server
```
npm start
```

## Sample Http Request

```
curl -H "Authorization: apiKeys" http://localhost:1774/api/weather?city=London&country=uk
```
There are 3 predefined api keys :
 - apiKeys
 - apiKeys1
 - apiKeys2

## Testing
Run eslint and test cases
```
npm test
```
