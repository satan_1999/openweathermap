import supertest from 'supertest'
import {expect, should, assert} from 'chai'
import Koa from 'koa'
import logger from 'koa-logger'
import router from '../server/routes/weather'
import authenticate from '../server/middlewares/authenticate'
import cookie from 'koa2-cookie-session'
import session  from 'koa-generic-session'
import { port } from '../server/config'


// Create Koa Application
const app = new Koa();
app.keys = ['secret', 'key'];


app.use(logger())
  .use(session())
  .use(cookie())
  .use(authenticate)
  .use(router.routes())
  .use(router.allowedMethods());

const request = supertest.agent(app.listen())
should()

describe('Test the weather API', () => {
  it('should get weather information for London UK', (done) => {
    request
      .get('/api/weather?city=London&country=uk')
      .set('Accept', 'application/json')
      .set('Authorization', 'apiKeys')
      .expect(200, (err, res) => {
        assert(res.body !== null, 'body is null')
      }).end(done())
  })

  it('should get empty weather information for wrong city', (done) => {
    request
      .get('/api/weather?city=xxxxx')
      .set('Accept', 'application/json')
      .set('Authorization', 'apiKeys')
      .expect(200, (err, res) => {
        expect(res.body === 'city not found')
      }).end(done())
  })
})

describe('Test authentication', () => {
  it('should get 401', (done) => {
    request
      .get('/api/weather?city=London&country=uk')
      .set('Accept', 'application/json')
      .set('Authorization', 'wrong api key')
      .expect(401, (err, res) => {
      }).end(done())
  })
})


