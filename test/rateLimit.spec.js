import supertest from 'supertest'
import {expect, should, assert} from 'chai'
import Koa from 'koa'
import logger from 'koa-logger'
import rateLimit from '../server/middlewares/rateLimit'
import router from '../server/routes/weather'
import cookie from 'koa2-cookie-session'
import session  from 'koa-generic-session'
import {port, max, intervalInMilliSeconds, redisPort, redisHost, enableRateLimit} from '../server/config'
import redis from 'redis'
import coRedis from 'co-redis'

const redisDB = redis.createClient(redisPort, redisHost)
const redisCo = coRedis(redisDB)


// Create Koa Application
const app = new Koa();
app.keys = ['secret', 'key'];


app.use(logger())
  .use(session())
  .use(cookie())
  .use(rateLimit)
  .use(router.routes())
  .use(router.allowedMethods());

const request = supertest.agent(app.listen())
should()

describe('Test RateLimit', () => {
  before(function () {
    // Flush Redis DB
    redisDB.flushdb(function (err, succeeded) {
      console.log(succeeded); // will be true if successfull
    });
  });

  after(function () {
    // Flush Redis DB
    redisDB.flushdb(function (err, succeeded) {
      console.log(succeeded); // will be true if successfull
    });
  });

  it('should be able to access api for the 1st', (done) => {
    request
      .get('/api/weather?city=London&country=uk')
      .set('Accept', 'application/json')
      .set('Authorization', 'apiKeys')
      .expect(200, (err, res) => {
        assert(res.headers['x-rateCount'] === 1 )
      }).end(done())
  })

  it('should be able to access api for the 2nd', (done) => {
    request
      .get('/api/weather?city=London&country=uk')
      .set('Accept', 'application/json')
      .set('Authorization', 'apiKeys')
      .expect(200, (err, res) => {
        assert(res.headers['x-rateCount'] === 2 )
      }).end(done())
  })

  it('should be able to access api for the 3th', (done) => {
    request
      .get('/api/weather?city=London&country=uk')
      .set('Accept', 'application/json')
      .set('Authorization', 'apiKeys')
      .expect(200, (err, res) => {
        assert(res.headers['x-rateCount'] === 3 )
      }).end(done())
  })

  it('should be able to access api for the 4th', (done) => {
    request
      .get('/api/weather?city=London&country=uk')
      .set('Accept', 'application/json')
      .set('Authorization', 'apiKeys')
      .expect(200, (err, res) => {
        assert(res.headers['x-rateCount'] === 4 )
      }).end(done())
  })

  it('should be able to access api for the 5th', (done) => {
    request
      .get('/api/weather?city=London&country=uk')
      .set('Accept', 'application/json')
      .set('Authorization', 'apiKeys')
      .expect(200, (err, res) => {
        assert(res.headers['x-rateCount'] === 5 )
      }).end(done())
  })

  it('should get 429 for the 6th', (done) => {
    request
      .get('/api/weather?city=London&country=uk')
      .set('Accept', 'application/json')
      .set('Authorization', 'apiKeys')
      .expect(429).end(done())
  })

  it('should be able to access api for a different api key', (done) => {
    request
      .get('/api/weather?city=London&country=uk')
      .set('Accept', 'application/json')
      .set('Authorization', 'apiKeys1')
      .expect(200, (err, res) => {
        assert(res.headers['x-rateCount'] === 1 )
      }).end(done())
  })
})


