export const port = process.env.PORT || 1774
export const redisPort = 6379
export const redisHost = '127.0.0.1'
export const baseApi = 'api'
export const enableRateLimit = false
export const max = 5
export const apiKeys = ['apiKeys', 'apiKeys1', 'apiKeys2']
export const intervalInMilliSeconds = 60 * 60 * 1000
export const weatherUrl = 'http://api.openweathermap.org/data/2.5/forecast?APPID=7f1f3d6abdf51a77fdb981334c8a71fb&q=%s,%s'
