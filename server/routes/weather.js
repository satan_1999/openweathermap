import 'babel-polyfill'
import Router from 'koa-router'
import { baseApi } from '../config'
import WeatherControllers from '../controllers/weatherController'

const api = 'weather'
const router = new Router();

router.prefix(`/${baseApi}/${api}`)

// GET /api/weather
router.get('/', WeatherControllers.findWeather)

export default router
