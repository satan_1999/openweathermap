import 'babel-polyfill'
import openWeatherService from '../services/openWeatherService'

class WeatherController {

  /* eslint-disable no-param-reassign*/

  /**
   * Find weather information for a city
   * @param {ctx} Koa Context
   */
  async findWeather(ctx) {
    try {
      const desc = await openWeatherService.fetchWeather(
        ctx, ctx.request.query.city, ctx.request.query.country)
      console.log(desc)
      ctx.body = desc;
    } catch (err) {
      console.error('There was an error', err);
      if (err.name === 'SubSystemError') {
        ctx.body = err.message;
        return ctx
      }
      ctx.throw(500)
    }
    return ctx
  }

  /* eslint-enable no-param-reassign */

}

export default new WeatherController()
