import { apiKeys } from '../config'


function send401(ctx) {
  ctx.status = 401;
  ctx.body = {
    message: 'Authentication Failed',
  };
}

export default async (ctx, next) => {
  const authHeader = ctx.headers.authorization
  if (ctx.request.user !== undefined && ctx.session.user === authHeader) {
    await next()
  } else if (authHeader && apiKeys.includes(authHeader)) {
    ctx.session.user = authHeader;
    await next()
  } else {
    ctx.session.user = null;
    send401(ctx)
  }

  return ctx;
}
