import redis from 'redis'
import coRedis from 'co-redis'
import { max, intervalInMilliSeconds, redisPort, redisHost, enableRateLimit } from '../config'


const redisDB = redis.createClient(redisPort, redisHost)
const redisCo = coRedis(redisDB)

export default async (ctx, next) => {
  if (enableRateLimit) {
    const rateLimitObj = {
      rateCount: await redisCo.get(`${ctx.session.user}-rateCount`),
      rateExpiry: await redisCo.get(`${ctx.session.user}-rateExpiry`),
    };

    if (rateLimitObj.rateExpiry && rateLimitObj.rateExpiry >= new Date().getTime()) {
      const rateCount = rateLimitObj.rateCount ? parseInt(rateLimitObj.rateCount, 10) : 1
      if (rateCount >= max) {
        ctx.status = 429
        ctx.body = { message: 'Over rateLimit' };
      } else {
        rateLimitObj.rateCount = rateCount + 1
      }
    } else {
      rateLimitObj.rateCount = 1
      rateLimitObj.rateExpiry = new Date(Date.now() + intervalInMilliSeconds).getTime()
    }
    redisCo.set(`${ctx.session.user}-rateCount`, rateLimitObj.rateCount);
    redisCo.set(`${ctx.session.user}-rateExpiry`, rateLimitObj.rateExpiry);
    ctx.cookies.set('X-rateCount', rateLimitObj.rateCount);
  }

  await next()
}

