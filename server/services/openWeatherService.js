import 'babel-polyfill'
import util from 'util'
import rp from 'request-promise'
import SubSystemError from '../errors/SubSystemError'
import { weatherUrl } from '../config'

class OpenWeatherService {
  async fetchWeather(ctx, city, country) {
    const url = util.format(weatherUrl, city, country)
    const options = { method: 'GET', url }
    const body = await rp(options).catch((err) => {
      const error = JSON.parse(err.error)
      throw new SubSystemError(error.message, err.statusCode)
    })

    const weatherInfo = JSON.parse(body)
    return weatherInfo.list[0].weather[0].description
  }
}

export default new OpenWeatherService();
