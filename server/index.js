import Koa from 'koa'
import logger from 'koa-logger'
import cookie from 'koa2-cookie-session'
import session from 'koa-generic-session'
import router from './routes/weather'
import rateLimit from './middlewares/rateLimit'
import authenticate from './middlewares/authenticate'

import { port } from './config'


// Create Koa Application
const app = new Koa();
app.keys = ['secret', 'key'];


app
  .use(logger())
  .use(session())
  .use(cookie())
  .use(authenticate)
  .use(rateLimit)
  .use(router.routes())
  .use(router.allowedMethods());


// Start the application
app.listen(port, () => console.log(`✅  The server is running at http://localhost:${port}/`))

export default app
